<?php
$adminEmail = 'cm@binovery.com';
if((isset($_POST['email']))&&($_POST['email'] != '')){
    
    switch($_GET['action']){
        case 'contact':{
            $email = strip_tags($_POST['email']);
            $phone = strip_tags($_POST['phone']);
            $message = strip_tags($_POST['message']);
            switch($_GET['lang']){
                case 'en':{
                    $adminHeader = 'Somebody used the contact form!';
                    $adminMessage = "E-mail : $email\n Phone : $phone\n Message : $message\n";
                    $userHeader = "Binovery - Thanks for filling in your details - we will be in touch shortly";
                    $userMessage = "Thanks for filling in your details - we will be in touch shortly";
                    mail($adminEmail, $adminHeader, $adminMessage);
                    mail($email, $userHeader, $userMessage);
                    echo ("Thanks for filling in your details - we will be in touch shortly");
                    break;
                }
                case 'ru':{
                    $adminHeader = 'Кто-то воспользовался формой обратной связи!';
                    $adminMessage = "E-mail : $email\n Телефон : $phone\n Сообщение : $message\n";
                    $userHeader = "Binovery - Спасибо за заполнение формы, мы скоро свяжемся с Вами";
                    $userMessage = "Спасибо за заполнение формы, мы скоро свяжемся с Вами";
                    mail($adminEmail, $adminHeader, $adminMessage);
                    mail($email, $userHeader, $userMessage);
                    echo ("Спасибо за заполнение формы, мы скоро свяжемся с Вами");
                    break;
                }
                case 'it':{
                    $adminHeader = 'Qualcuno ha utilizzato il modulo di contatto!';
                    $adminMessage = "E-mail : $email\n Telefono : $phone\n Messaggio : $message\n";
                    $userHeader = "Binovery - Grazie per riempire i tuoi dati - saremo in contatto a breve";
                    $userMessage = "Grazie per riempire i tuoi dati - saremo in contatto a breve";
                    mail($adminEmail, $adminHeader, $adminMessage);
                    mail($email, $userHeader, $userMessage);
                    echo ("Grazie per riempire i tuoi dati - saremo in contatto a breve");
                    break;
                }
            }
            
            break;
        }
        case 'framework':{
            $email = strip_tags($_POST['email']);
            switch($_GET['lang']){
                case 'en':{
                    $adminHeader = 'Somebody wants to know more about our framework!';
                    $adminMessage = "E-mail : $email\n <br>";
                    $userHeader = "Binovery Framework - Thanks for filling in your details - we will be in touch shortly";
                    $userMessage = "Thanks for filling in your details - we will be in touch shortly";
                    mail($adminEmail, $adminHeader, $adminMessage);
                    mail($email, $userHeader, $userMessage);
                    echo ("Thanks for your interest to our framework - we will be in touch shortly");
                    break;
                }
                case 'ru':{
                    $adminHeader = 'Кто-то хочет узнать больше о нашем фреймворке!';
                    $adminMessage = "E-mail : $email\n <br>";
                    $userHeader = "Binovery Фреймворк - Спасибо за заполнение формы, мы скоро свяжемся с Вами";
                    $userMessage = "Спасибо за заполнение формы, мы скоро свяжемся с Вами";
                    mail($adminEmail, $adminHeader, $adminMessage);
                    mail($email, $userHeader, $userMessage);
                    echo ("Спасибо за интерес к нашему фреймворку, мы скоро свяжемся с Вами");
                    break;
                }
                case 'it':{
                    $adminHeader = 'Qualcuno vuole sapere di più sul nostro framework!';
                    $adminMessage = "E-mail : $email\n <br>";
                    $userHeader = "Binovery Framework - Grazie per riempire i tuoi dati - saremo in contatto a breve";
                    $userMessage = "Grazie per riempire i tuoi dati - saremo in contatto a breve";
                    mail($adminEmail, $adminHeader, $adminMessage);
                    mail($email, $userHeader, $userMessage);
                    echo ("Grazie per riempire i tuoi dati - saremo in contatto a breve");
                    break;
                }
            
            }
            break;
        }
            
        case 'subscribe':{
            $email = strip_tags($_POST['email']);
            switch($_GET['lang']){
                case 'en':{
                    $adminHeader = 'Somebody wants to subscribe to our newsletter!';
                    $adminMessage = "E-mail : $email\n";
                    $userHeader = "Binovery - Thanks for signing up";
                    $userMessage = "Thanks for signing up";
                    mail($adminEmail, $adminHeader, $adminMessage);
                    mail($email, $userHeader, $userMessage);
                    echo ("Thanks for subscribing!");
                    break;
                }
                case 'ru':{
                    $adminHeader = 'Кто-то хочет подписаться на нашу рассылку!';
                    $adminMessage = "E-mail : $email\n <br>";
                    $userHeader = "Binovery - Спасибо за подписку!";
                    $userMessage = "Спасибо за подписку на рассылку, мы скоро свяжемся с Вами";
                    mail($adminEmail, $adminHeader, $adminMessage);
                    mail($email, $userHeader, $userMessage);
                    echo ("Спасибо за подписку на рассылку, мы скоро свяжемся с Вами");
                    break;
                }
                case 'it':{
                    $adminHeader = 'Qualcuno vuole iscriversi alla nostra newsletter!';
                    $adminMessage = "E-mail : $email\n <br>";
                    $userHeader = "Binovery -  Grazie per esserti registrato";
                    $userMessage = "Grazie per esserti registrato";
                    mail($adminEmail, $adminHeader, $adminMessage);
                    mail($email, $userHeader, $userMessage);
                    echo ("Grazie per esserti registrato");
                    break;
                }
            
            }
            
        }
    }
}
else{
    echo("You have not entered an email. Please, type your email in the field and try again");
}
?>